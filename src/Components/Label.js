import React, { Component } from "react";

class Label extends Component {
  render() {
    return <label> {this.props.name} </label>;
  }
}

export default Label;
