import React, { Component } from "react";

class Button extends Component {
  render() {
    return (
      <button
        id={this.props.id}
        disabled={this.props.disabled}
        onClick={this.props.onCick}
        name={this.props.name}

      >
        {this.props.name}
      </button>
    );
  }
}

export default Button;
