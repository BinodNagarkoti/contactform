import React, { Component } from "react";
import Button from "./Components/Button";
import Input from "./Components/Input";
import Label from "./Components/Label";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      message: "",
      contact: "",
      list: []
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onChange = ({ target }) => {
    const { name, value } = target;
    this.setState({
      [name]: value
    });
  };
  onSubmit = () => {
    console.log("submit", "onSubmit Triggred");
    this.state.list.map(item => {
      console.log(item);
      return 0;
    });
    this.setState({
      ...this.state,
      list: this.state.list.concat({
        name: this.state.name,
        email: this.state.email,
        message: this.state.message,
        contact: this.state.contact
      })
    });
  };

  render() {
    // const List = this.state.list.map((value, index) => <li> {value} </li>);
    return (
      <div className="App">
        <header>
          <h1> React From</h1>
        </header>

        <Label name="Name" />
        <Input
          type="text"
          onChange={this.onChange}
          value={this.state.name}
          name="name"
          disabled={false}
        />
        <br />

        <Label name="email" />
        <Input
          type="email"
          onChange={this.onChange}
          value={this.state.email}
          name="email"
          disabled={false}
        />
        <br />

        <Label name="message" />
        <Input
          type="textarea"
          onChange={this.onChange}
          value={this.state.message}
          name="message"
          disabled={false}
        />
        <br />

        <Label name="contact" />
        <Input
          type="number"
          onChange={this.onChange}
          value={this.state.contact}
          name="contact"
          disabled={false}
        />
        <br />
        <Input
          type="submit"
          onClick={this.onSubmit}
          name="Submit"
          id="button"
          disabled={this.state.name === "" ? true : false}
        />
        <br />
        <br />
        {this.state.list.map((value, index) => (
          <div key={index}>
            <li> {value.name} </li>
            <li>{value.email}</li>
            <li>{value.message} </li>
            <li>{value.contact}</li>
            <hr />
          </div>
        ))}
      </div>
    );
  }
}

export default App;
